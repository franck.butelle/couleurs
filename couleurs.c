/* ici les guillemets montre que le fichier à inclure est 
 * dans le même répertoire */
#include "couleurs.h"

/* ici les fonctions dont les prototypes ont
 * été déclarés dans couleurs.h */

void print_fgcolor(const int c, const char *texte) {
	printf(ANSI_CSI_FG "%dm%s" ANSI_RESET, c, texte);
}

void print_fgrouge(const char *texte) {
    print_fgcolor(ROUGE,texte);
}

void print_fgvert(const char *texte) {
	print_fgcolor(VERT,texte);
}

char *bgcolor(const int c, const char *texte) {
	static char s[256];
	sprintf(s, ANSI_CSI_BG "%dm%s" ANSI_RESET, c, texte);
	return(s);
}

char *bgfgcolor(const int cbg, const int cfg, const char *texte) {
	static char s[256];
    sprintf(s, ANSI_CSI_BG "%dm" ANSI_CSI_FG "%dm%s" ANSI_RESET, cbg, cfg, texte);
    return(s);
}

char *bgbleu(const char *texte) {
	return(bgcolor(BLEU,texte));
}

