#include "couleurs.h"

int main() {
    // Utilisation de la fonction rouge() pour afficher un message en rouge
    print_fgrouge("Ceci est un message en rouge.\n");
	print_fgvert("Ceci est un message en vert.\n");
	print_fgcolor(3,"ceci est un message en code couleur 3.\n");

	printf("%s\n",bgbleu("ceci est un message sur fond bleu."));
	printf("%s\n",bgcolor(MAGENTA,"ceci est un message sur fond magenta."));

    return 0;
}

