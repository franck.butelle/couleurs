/* Ces 2 premières lignes sont importantes pour ne pas
 * déclarer plusieurs fois les prototypes des fonctions et les constantes
 * si on ne fait pas attention et on fait plusieurs
 * includes (dans des programmes plus complexes bien sûr).*/

#ifndef COULEURS_H
#define COULEURS_H

#include <stdio.h>

/* Version 3 bits par couleur*/
  // CSI=Control Sequence Introducer
#define ANSI_CSI     "\x1b["
#define ANSI_RESET   "\x1b[00m"

/* foreground */
#define ANSI_CSI_FG  "\x1b[1;3"
/* background */
#define ANSI_CSI_BG  "\x1b[1;4"

#define ROUGE 	1
#define VERT  	2
#define JAUNE 	3
#define BLEU  	4
#define MAGENTA 5
#define CYAN    6


/* Les prototypes des fonctions */
void print_fgcolor(const int c, const char *texte);
void print_fgrouge(const char *texte);
void print_fgvert(const char *texte);

char * bgcolor(const int c, const char *texte);
char * bgbleu(const char *texte);

char * bgfgcolor(const int cbg, const int cfg, const char *texte);
#endif

